# Backup Shared Hosting

This script aims at facilitating the backup of files and databases on a shared host (this was originally designed for Hostgator but should be easy to apply to Dreamhost, Bluehost, A Small Orange, etc...). In order to adapt the script to your environment, it is necessary to update all content that is in `[]`.

The last 5 backups of every archives (database or files) are kept. A MD5 sum of each archive is registered in a log file so that only new archives are kept.

## Backup of Database

In order to back up the databases, it is recommended to have a special account for the backup only (in other words, an account that doesn't need write permissions on the DB). The same account can be used to backup multiple databases.

## Backup of Files & Folders

The script focuses on creating archives (more exactly tar files) of specific files and folders.

## Read More

An article going through this process is available on my [personal website](http://jimmybonney.com/articles/backup_website_shared_hosting/).

## License

Licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.html).