#!/bin/sh

# Redirect the stdout and stderr to the log file
exec 1> /home/[YOURACCOUNT]/backups/files/exec_logs.txt 2>&1

# Global variables for the complete script
THEACCOUNT="[YOURACCOUNT]"
THEDBUSER="[YOURDBUSER]"
THEDBPW="[YOURDBPASSWORD]"
THEDATE=`date +%d%m%y%H%M`

# md5 function to verify if archive has changed since last backup
# the function except the following arguments
# $1: a brief description of the file being checked
# $2: the path to the MD5 file -> currently not used
# $3: the path to the archive to verify against the checksum
# $4: the path to the log file
md5check()
{
  DESCRIPTION=$1
  MD5FILE=$2
  ARCHIVE=$3
  LOG=$4
  # Verify if previous backups were made
  if [ -f $LOG ];
  then
    echo "A previous archive exists for $DESCRIPTION"
    # Grab the date at which the previous script was run
    # Get the last line of the log file and take the first string (it contains the date)
    PREVIOUSDATE=`tail -1 $LOG | awk '{print $1}'`
    echo "Previous backup ran at: $PREVIOUSDATE"
    # Grab the previous md5sum
    SUM=`tail -1 $LOG | awk '{print $2}'`
    # echo "Sum: $SUM"
    # Grab the previous filename
    FILE=`tail -1 $LOG | awk '{print $3}'`
    # echo "File: $FILE"
    # Set the sum and filename in a tmp file
    # echo "${SUM}  ${FILE}" > previous_md5.txt
    # echo "Previous checksum: "; cat previous_md5.txt
    # Verify if the previous checksum applies to the current archive
    # The output of md5sum is as follow 'sum  file': notice the two (2) spaces between the sum and the filename
    echo "${SUM}  ${ARCHIVE}" > tmp_md5.txt
    # echo "Verify checksum on: "; cat tmp_md5.txt
    CHECKSUMRESULT=`md5sum -c tmp_md5.txt | awk '{print $2}'`
    if [ "$CHECKSUMRESULT" = "OK" ]
    then
      echo "Checksums are the same: archive has not changed since last run"
      echo "${THEDATE} ${SUM} ${FILE} => keep previous file" >> $LOG
      echo "Delete newly created archive ($ARCHIVE)"
      echo "Keep previous archive ($FILE)"
      rm $ARCHIVE
    else
      echo "Checksums are not the same: archive has changed since last run"
      NEWSUM=`md5sum $ARCHIVE`
      # echo "Checksum: $NEWSUM"
      SUM=`echo ${NEWSUM} | awk '{print $1}'`
      # echo "Sum: $SUM"
      FILE=`echo ${NEWSUM} | awk '{print $2}'`
      # echo "File: $FILE"
      echo "${THEDATE} ${SUM} ${FILE} => new backup file" >> $LOG
      echo "Keep new archive ($ARCHIVE)"
    fi
  else
    echo "Create new log file for $DESCRIPTION"
    NEWSUM=`md5sum $ARCHIVE`
    # echo "Checksum: $NEWSUM"
    SUM=`echo ${NEWSUM} | awk '{print $1}'`
    # echo "Sum: $SUM"
    FILE=`echo ${NEWSUM} | awk '{print $2}'`
    # echo "File: $FILE"
    echo "${THEDATE} ${SUM} ${FILE} => new backup file" > $LOG
  fi
}

# Clean old backup files
# $1: file pattern
clean_old_backups()
{
  PATTERN=$1
  # Remember current directory
  CURRENTDIR=`pwd`
  cd /home/${THEACCOUNT}/backups/files
  # Remove old backups
  (ls -t|grep $PATTERN|head -n 5;ls|grep $PATTERN)|sort|uniq -u|xargs rm
  # Get back to the directory
  cd $CURRENTDIR
}

# Backup of main website
# Hosted on Hostgator under the handling [YOURACCOUNT]
THEDB="[NAMEOFTHEDATABASE]"
THEOTHERDB="[NAMEOFANOTHERDATABASE]"
THEDBUSER="[THEDATABASEUSER]"
THEDBPW="[THEPASSWORDFORTHEDBUSER]"

echo ''
echo '###################################################################'
echo '                      Backup hostgator server                      '
echo "                      Execution date: ${THEDATE}                   "
echo '###################################################################'
echo ''
# Dump the DB
echo ''
echo '-------------------------------------------------------------------'
echo "Dump the DB"
THEDBZIP="/home/${THEACCOUNT}/backups/files/dbbackup_${THEDB}_${THEDATE}.bak.gz"
mysqldump -u $THEDBUSER -p${THEDBPW} $THEDB | gzip > $THEDBZIP
# Verify the MD5 sum
md5check "The DB" unused.txt $THEDBZIP "/home/${THEACCOUNT}/backups/files/dbbackup_${THEDB}_logs.txt"
clean_old_backups "dbbackup_${THEDB}"
echo '-------------------------------------------------------------------'
echo ''

# Dump the other DB
echo ''
echo '-------------------------------------------------------------------'
echo "Dump the other DB"
THEOTHERZIP="/home/${THEACCOUNT}/backups/files/dbbackup_${THEOTHERDB}_${THEDATE}.bak.gz"
mysqldump -u $THEDBUSER -p${THEDBPW} $THEOTHERDB | gzip > $THEOTHERZIP
# Verify the MD5 sum
md5check "The other DB" unused.txt $THEOTHERZIP "/home/${THEACCOUNT}/backups/files/dbbackup_${THEOTHERDB}_logs.txt"
clean_old_backups "dbbackup_${THEOTHERDB}"
echo '-------------------------------------------------------------------'
echo ''

# Save the necessary files for the main website
echo ''
echo '-------------------------------------------------------------------'
THESITE="[SITENAME]"
THETARFILE="/home/${THEACCOUNT}/backups/files/sitebackup_${THESITE}_${THEDATE}.tar"
THEWEBFOLDER="/home/${THEACCOUNT}/www"
echo "Save files related to the main website"
echo "Create the archive"
echo "Add a first folder to the archive"
tar --create --exclude=.svn --file=$THETARFILE -C $THEWEBFOLDER folder1
echo "Add a second folder to the archive"
tar --append --exclude=.svn --file=$THETARFILE -C $THEWEBFOLDER folder2
echo "Add all .php file to the archive"
find $THEWEBFOLDER/* -maxdepth 0 -name '*.php' -exec basename "{}" \; | tar --append --exclude=.svn --file=$THETARFILE -C $THEWEBFOLDER -T -
echo "Add all .txt file to the archive"
find $THEWEBFOLDER/* -maxdepth 0 -name '*.txt' -exec basename "{}" \; | tar --append --exclude=.svn --file=$THETARFILE -C $THEWEBFOLDER -T -
echo "Add img folder to the archive"
tar --append --exclude=.svn --file=$THETARFILE -C $THEWEBFOLDER img
echo "Add js folder to the archive"
tar --append --exclude=.svn --file=$THETARFILE -C $THEWEBFOLDER js
# Verify the MD5 sum
md5check "d-sight.com" unused.txt $THETARFILE "/home/${THEACCOUNT}/backups/files/sitebackup_${THESITE}_logs.txt"
clean_old_backups "sitebackup_${THESITE}"
# Compressing the archive result in different md5sums even if the content of the zip hasn't changed
# echo "Compress the archive"
# gzip $THETARFILE
echo '-------------------------------------------------------------------'
echo ''

# Save the necessary files for a sub website
echo ''
echo '-------------------------------------------------------------------'
THESITE="another site"
THETARFILE="/home/${THEACCOUNT}/backups/files/sitebackup_${THESITE}_${THEDATE}.tar"
THEWEBFOLDER="/home/${THEACCOUNT}/www"
echo "Save files related to the other website"
echo "Create the archive"
tar --create --exclude=.svn --file=$THETARFILE -C $THEWEBFOLDER some-folder
# Verify the MD5 sum
md5check "the other site" unused.txt $THETARFILE "/home/${THEACCOUNT}/backups/files/sitebackup_${THESITE}_logs.txt"
clean_old_backups "sitebackup_${THESITE}"
# Compressing the archive result in different md5sums even if the content of the zip hasn't changed
# echo "Compress the archive"
# gzip $THETARFILE
echo '-------------------------------------------------------------------'
echo ''